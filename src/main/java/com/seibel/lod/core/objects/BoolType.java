package com.seibel.lod.core.objects;

public final class BoolType {
    public static final BoolType TRUE = new BoolType();
    public static final BoolType FALSE = null;
    private BoolType() {}
}
