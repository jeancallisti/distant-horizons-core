package com.seibel.lod.core.wrapperInterfaces.modAccessor;

import com.seibel.lod.core.handlers.dependencyInjection.IBindable;

/**
 * @author Leetom
 * @version 3-5-2022
 */
public interface IModAccessor extends IBindable {
	String getModName();
}
