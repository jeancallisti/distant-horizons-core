package com.seibel.lod.core.enums.rendering;

public enum HeightFogMixMode {
    BASIC,
    IGNORE_HEIGHT,
    ADDITION,
    MAX,
    MULTIPLY,
    INVERSE_MULTIPLY,
    LIMITED_ADDITION,
    MULTIPLY_ADDITION,
    INVERSE_MULTIPLY_ADDITION,
    AVERAGE,
}
